# Aprender a vender downloader

Python app to get all video urls from "Aprender a vender", a small helper that use youtube-dl to download the videos is added.


## Requirements

Before we start you are required to have python 3.9 and pipenv installed in your computer, before running this program you need to run

    pipenv install 

this will install all the required libraries to use this program, after that you need to enable the python enviroment running 

    pipenv shell

so that you can run our program.

You have to create a credentials.json file where you will put your username and password, it looks something like this

    {
        "username": "Your-Username",
        "password": "Your-Password"
    }

and you need to create a json formatted list of objects with 'name' and 'url' in course_list.json as well.

    [
        {
            "name": "Método Master Digital – Más Clientes, Más Ventas",
            "url":"https://miembros.aprendeavender.net/courses/metodo-master-digital-mas-ventas-menos-seguidores/"
        }
    ]


## How to run

This program generates a videos.json file that contains al information about the videos in the course "Método Master Digital - Más Clientes, Más Ventas", you need to put your credentials in the credentials.json and run the command.

    python app.py

It will open a windows chrome window with selenium and then it will start to iterate through the course to get all the links, after going through every lesson them it will store the info in a videos.json file.

A helper program is included so you can use youtube-dl to download the videos (if all are from youtube), just run 

    python download.py

and it will start to download every video.
