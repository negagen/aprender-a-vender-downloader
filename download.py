import json
from youtube_dl import YoutubeDL

with open('videos.json') as f:
    courses = json.load(f)

for course in courses:
    youtube_urls = []
    for video in course['videos']:    
        try:
            if(video['video_type']=="youtube"):
                youtube_urls.append(video['youtube_url'])
        except KeyError:
            pass
    
    opts = {
        "outtmpl":"videos/"+course['name']+"/%(autonumber)s-%(title)s"
    }

    with YoutubeDL() as ydl:
        ydl.download(youtube_urls)
