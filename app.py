from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
import json
import asyncio



async def login(driver: webdriver.Chrome):
    driver.execute_script("return document.getElementById('customerly-container').remove()")
    with open("./credentials.json") as f:
        credentials = json.load(f)


    input_username = driver.find_element(By.XPATH, "/html/body/div[1]/div/div/section/div/div/div[1]/div/div/div[3]/div/div/div/form/div/div[1]/input")
    input_password = driver.find_element(By.XPATH, "/html/body/div[1]/div/div/section/div/div/div[1]/div/div/div[3]/div/div/div/form/div/div[2]/span/input")
    login_button = driver.find_element(By.XPATH, "/html/body/div[1]/div/div/section/div/div/div[1]/div/div/div[3]/div/div/div/form/div/p[1]/input")


    input_username.send_keys(credentials['username'])
    input_password.send_keys(credentials['password'])

    
    login_button.click()

async def open_course(driver, url):
    driver.get(url)

async def start_course(driver):
    button_start_course = driver.find_element(By.XPATH, "/html/body/div[1]/div/div/div/div/main/div/div/div[2]/div[2]/div/div/div[2]/div[3]/div/a")
    button_start_course.click()

async def get_content(driver):
    video = driver.find_element(By.CSS_SELECTOR, ".elementor-widget-video")
    video_opts = json.loads(video.get_attribute('data-settings'))

    return video_opts

async def next(driver):
    next_btn = driver.find_element(By.CSS_SELECTOR, "#learndash-course-header > div.flex.bb-position > div.sfwd-course-nav > div.learndash_next_prev_link > a.next-link")
    next_btn.click()

async def get_videos(driver, course):
    # Open to course!
    await open_course(driver, course)
    
    # Click on start course!
    await start_course(driver)

    videos = []

    while True:
        try:
            video_data = await get_content(driver)
            videos.append(video_data)
        except NoSuchElementException:
            videos.append({"error":"no-video"})
        
        try:
            await next(driver)
        except NoSuchElementException:
            break

    return videos

async def main():
    service = Service(ChromeDriverManager().install())
    driver = webdriver.Chrome(service=service)
    driver.get("https://miembros.aprendeavender.net/ingreso/")

    # Login user!
    await login(driver)

    with open('course_list.json') as f:
        courses = json.load(f)
    
    for course in courses:
        courses["videos"] = get_videos(driver, course)
    
    with open('videos.json','w') as f:
            json.dump(courses, f)

    driver.close()

if __name__ == "__main__":
    asyncio.run(main())

